from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views.generic import CreateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView


from projects.models import Project


# Create your views here.
class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    context_object_name = "projects"
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "project"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/new.html"
    fields = ["name", "description", "members"]
    # success_url = reverse_lazy("show_project")

    def form_valid(self, form):
        # showing the user custom form
        instance = form.save()
        # saving the form as an instance

        instance.members.add(self.request.user)
        # we're adding the user as a member in the project

        return redirect("show_project", pk=instance.pk)

        # render a page with all the information user entered as a detail page.
        # return

        # return HttpResponseRedirect(
        #     reverse("show_project", kwargs={"form": form})
        # )

        # return render(self.request, "show_project", instance.pk)
