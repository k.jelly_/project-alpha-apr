from django.contrib import admin

from projects.models import (
    Project,
)

from tasks.models import (
    Task,
)


class ProjectAdmin(admin.ModelAdmin):
    pass


class TaskAdmin(admin.ModelAdmin):
    pass


# Register your models here.

admin.site.register(Project, ProjectAdmin)
admin.site.register(Task, TaskAdmin)
